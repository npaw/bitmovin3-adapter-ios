//
//  YBBitmovinAdsAdapter.swift
//  YouboraBitmovinAdapter
//
//  Created by Elisabet Massó on 21/5/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraLib
import BitmovinPlayer

public class YBBitmovinAdsAdapter: YBPlayerAdapter<AnyObject> {
    
    private var adDuration: Double?
    private var adPosition: String?
    private var adProvider: String?
    private var lastPlayhead: Double?

    private override init() {
        super.init()
    }
    
    public init(player: Player) {
        super.init(player: player)
    }
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? Player {
            player.add(listener: self)
        }
        resetValues()
    }
    
    public override func unregisterListeners() {
        if let player = player as? Player {
            player.remove(listener: self)
        }
        resetValues()
        super.unregisterListeners()
    }
    
    func resetValues() {
        adDuration = nil
        adPosition = nil
        adProvider = nil
        lastPlayhead = nil
    }
    
    // MARK: - Getters
    
    public override func getAdProvider() -> String? {
        return adProvider
    }
    
    public override func getDuration() -> NSNumber? {
        guard let adDuration = adDuration else { return super.getDuration() }
        return NSNumber(value: adDuration)
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let lastPlayhead = lastPlayhead else { return super.getPlayhead() }
        return NSNumber(value: lastPlayhead)
    }
    
    public override func getPosition() -> YBAdPosition {
        guard let adPosition = adPosition, adPosition != "" else { return .unknown }
        if adPosition == "pre" {
            return .pre
        } else if adPosition == "post" {
            return .post
        }
        return .mid
    }
    
    public override func fireStop(_ params: [String : String]?) {
        super.fireStop(params)
        resetValues()
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdsAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName()
    }
    
    public override func getVersion() -> String {
        return Constants.getAdsAdapterVersion()
    }
    
}

extension YBBitmovinAdsAdapter: PlayerListener {
    
    public func onPlay(_ event: PlayEvent, player: Player) {
        if player.isAd {
            fireResume()
        }
    }
    
    public func onPaused(_ event: PausedEvent, player: Player) {
        if player.isAd {
            firePause()
        }
    }
    
    public func onTimeChanged(_ event: TimeChangedEvent, player: Player) {
        if player.isAd && player.currentTime > 0 {
            lastPlayhead = event.currentTime
        }
    }
    
    public func onAdStarted(_ event: AdStartedEvent, player: Player) {
        adPosition = event.position
        adProvider = event.clientType == .ima ? "IMA" : "Unknown"
        adDuration = event.duration
        fireResume()
        fireStart()
        fireJoin()
    }
    
    public func onAdFinished(_ event: AdFinishedEvent, player: Player) {
        fireStop()
    }
    
    public func onAdQuartile(_ event: AdQuartileEvent, player: Player) {
        switch event.adQuartile {
            case .firstQuartile:
                fireQuartile(1)
            case .midpoint:
                fireQuartile(2)
            case .thirdQuartile:
                fireQuartile(3)
            @unknown default:
                break
        }
    }
    
    public func onAdBreakStarted(_ event: AdBreakStartedEvent, player: Player) {
        fireAdBreakStart()
    }
    
    public func onAdBreakFinished(_ event: AdBreakFinishedEvent, player: Player) {
        fireAdBreakStop()
    }
    
    public func onAdSkipped(_ event: AdSkippedEvent, player: Player) {
        fireStop(["skipped" : "true"])
    }
    
    public func onAdClicked(_ event: AdClickedEvent, player: Player) {
        if let url = event.clickThroughUrl?.absoluteString {
            fireClick(["adUrl" : url])
        } else {
            fireClick()
        }
    }
    
    public func onAdError(_ event: AdErrorEvent, player: Player) {
        fireFatalError(withMessage: event.message, code: "\(event.code)", andMetadata: nil)
    }
    
    public func onAdManifestLoaded(_ event: AdManifestLoadedEvent, player: Player) {
        fireAdManifest(nil)
    }
    
}
