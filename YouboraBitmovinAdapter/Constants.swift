//
//  Constants.swift
//  YouboraBitmovinAdapter
//
//  Created by Elisabet Massó on 21/5/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation

final class Constants {
    
    static private var bitmovinAdapterVersion = "6.6.1"
    
    static func getAdapterName() -> String {
        return "\(getName())-\(getPlatform())"
    }
    
    static func getAdapterVersion() -> String {
        return "\(getVersion())-\(getAdapterName())"
    }
    
    static func getAdsAdapterName() -> String {
        return "\(getName())-Ads-\(getPlatform())"
    }
    
    static func getAdsAdapterVersion() -> String {
        return "\(getVersion())-\(getAdsAdapterName())"
    }
    
    static func getName(version: Bool = true) -> String {
        return version ? "Bitmovin3" : "Bitmovin"
    }
    
    static func getPlatform() -> String {
        #if os(tvOS)
        return "tvOS"
        #else
        return "iOS"
        #endif
    }
    
    static func getVersion() -> String {
        return bitmovinAdapterVersion
    }
    
}
