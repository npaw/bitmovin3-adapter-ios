//
//  YBBitmovinAdapter.swift
//  YouboraBitmovinAdapter
//
//  Created by Elisabet Massó on 20/5/21.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraLib
import BitmovinPlayer

public class YBBitmovinAdapter: YBPlayerAdapter<AnyObject> {
    
    private var adPosition: String?
    private var lastPlayhead: Double = 0
        
    private override init() {
        super.init()
    }
    
    public init(player: Player) {
        super.init(player: player)
    }
    
    
    public override func registerListeners() {
        super.registerListeners()
        if let player = player as? Player {
            player.add(listener: self)
        }
        monitorPlayhead(withBuffers: true, seeks: true, andInterval: 800)
        resetValues()
    }
    
    public override func unregisterListeners() {
        if let player = player as? Player {
            player.remove(listener: self)
        }
        monitor?.stop()
        resetValues()
        super.unregisterListeners()
    }
    
    func resetValues() {
        adPosition = nil
        lastPlayhead = 0
    }
    
    // MARK: - Getters
    
    public override func getBitrate() -> NSNumber? {
        guard let player = player as? Player else { return super.getBitrate() }
        if let bitrate = player.videoQuality?.bitrate, bitrate > 0 {
            return NSNumber(value: bitrate)
        }
        return super.getBitrate()
    }
    
    public override func getDuration() -> NSNumber? {
        guard let player = player as? Player else { return super.getDuration() }
        return NSNumber(value: player.duration)
    }
    
    public override func getIsLive() -> NSValue? {
        guard let player = player as? Player else { return super.getIsLive() }
        if player.isLive {
            return NSNumber(value: true)
        } else if player.duration != 0 {
            return NSNumber(value: false)
        }
        return super.getIsLive()
    }
    
    public override func getPlayhead() -> NSNumber? {
        guard let player = player as? Player else { return super.getPlayhead() }
        if player.isAd {
            return NSNumber(value: lastPlayhead)
        }
        return NSNumber(value: player.currentTime)
    }
    
    public override func getPlayrate() -> NSNumber {
        guard let player = player as? Player else { return super.getPlayrate() }
        if flags.paused {
            return 0
        }
        return NSNumber(value: player.playbackSpeed)
    }
    
    public override func getRendition() -> String? {
        if let player = player as? Player, let videoQuality = player.videoQuality, let bitrate = getBitrate() {
            return YBYouboraUtils.buildRenditionString(withWidth: Int32(videoQuality.width), height: Int32(videoQuality.height), andBitrate: bitrate.doubleValue)
        }
        return super.getRendition()
    }
    
    public override func getResource() -> String? {
        guard let player = player as? Player, let source = player.source else { return super.getResource() }
        if source.sourceConfig.type == .dash || source.sourceConfig.type == .hls {
            return source.sourceConfig.url.absoluteString
        }
        return super.getResource()
    }
    
    public override func getTitle() -> String? {
        guard let player = player as? Player, let source = player.source else { return super.getTitle() }
        return source.sourceConfig.title
    }
    
    public override func fireStop(_ params: [String : String]?) {
        resetValues()
        super.fireStop(params)
    }
    
    // MARK: - Adapter info
    
    public override func getPlayerName() -> String? {
        return Constants.getAdapterName()
    }
    
    public override func getPlayerVersion() -> String? {
        return Constants.getName(version: false)
    }
    
    public override func getVersion() -> String {
        return Constants.getAdapterVersion()
    }

}

extension YBBitmovinAdapter: PlayerListener {
    
    public func onPlay(_ event: PlayEvent, player: Player) {
        fireResume()
        fireStart()
    }
    
    public func onPlaying(_ event: PlayingEvent, player: Player) {
        if !player.isAd {
            fireJoin()
        }
    }
    
    public func onPaused(_ event: PausedEvent, player: Player) {
        firePause()
    }
    
    public func onTimeChanged(_ event: TimeChangedEvent, player: Player) {
        if !player.isAd {
            lastPlayhead = event.currentTime
        }
    }
    
    public func onSeek(_ event: SeekEvent, player: Player) {
        fireSeekBegin()
    }
    
    public func onPlaybackFinished(_ event: PlaybackFinishedEvent, player: Player) {
        if !player.isAd {
            fireStop()
        } else {
            firePause()
        }
    }
    
    public func onPlayerError(_ event: PlayerErrorEvent, player: Player) {
        fireFatalError(withMessage: event.message, code: "\(event.code)", andMetadata: nil)
    }
    
    public func onSourceError(_ event: SourceErrorEvent, player: Player) {
        fireFatalError(withMessage: event.message, code: "\(event.code)", andMetadata: nil)
    }
    
    public func onSourceUnloaded(_ event: SourceUnloadedEvent, player: Player) {
        fireStop()
    }
    
    public func onAdStarted(_ event: AdStartedEvent, player: Player) {
        adPosition = event.position
    }
    
    public func onAdBreakFinished(_ event: AdBreakFinishedEvent, player: Player) {
        if adPosition == "post" {
            fireStop()
        }
    }
    
    public func onEvent(_ event: Event, player: Player) {
        YBSwiftLog.debug("onEvent: \(event.name)")
    }
    
}
