# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [6.6.1] - 2021-12-14
### Fixed
- Xcode 13 framework version

## [6.6.0] - 2021-09-27
### Added
- Source errors

## [6.5.0] - 2021-06-08
### Added
- Release version