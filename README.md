# YouboraBitmovinAdapter #

A framework that will collect several video events from the BitmovinPlayer and send it to the back end

# Installation #

#### CocoaPods ####

Add this sources into your Podfile

```bash
source 'https://bitbucket.org/npaw/ios-sdk-podspecs.git'
source 'https://github.com/bitmovin/cocoapod-specs.git'
source 'https://github.com/CocoaPods/Specs.git'
```

Insert this pod to your target

```bash
pod 'YouboraBitmovin3Adapter'
```

Then run the command

```bash
pod install
```

#### Carthage ####

Create your Cartfile and insert 

```bash
git "https://bitbucket.org/npaw/bitmovin3-adapter-ios.git"
```

Then run the command

* For Xcode 12.x or major:

```bash
carthage update --use-xcframeworks
```

* For Xcode 11.x or minor:

```bash
carthage update
```

Go to **{YOUR_SCHEME} > Build Settings > Framework Search Paths** and add:

* For Xcode 12.x or major: **\$(PROJECT_DIR)/Carthage/Build**
* For Xcode 11.x or minor: **\$(PROJECT_DIR)/Carthage/Build/{iOS, Mac, tvOS or the platform of your scheme}**

## How to use ##

### Start plugin and options ###

```swift
//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
    let options = YBOptions()
    options.contentResource = "http://example.com"
    options.accountCode = "accountCode"
    options.adResource = "http://example.com"
    options.contentIsLive = NSNumber(value: false)
    return options;
}
    
lazy var plugin = YBPlugin(options: options)
```

For more information about the options you can check [here](http://developer.nicepeopleatwork.com/apidocs/ios6/Classes/YBOptions.html)

### YBBitmovinAdapter & YBBitmovinAdAdapter ###

```swift
import YouboraBitmovin3Adapter
...

//Once you have your player and plugin initialized you can set the adapter
plugin.adapter = YBBitmovinAdapter(player: player)

...

//If you want to setup the ads adapter as well
plugin.adsAdapter = YBBitmovinAdsAdapter(player: player)

...

// When the view is going to be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
plugin.fireStop()
plugin.removeAdapter()
plugin.removeAdsAdapter()
```

## Run samples project ##

###### Via cocoapods ######

Navigate to Samples folder and execute: 

```bash
pod install
```

###### Via Carthage ######

Navigate to the root folder and execute: 

* For Xcode 12.x or major:

```bash
carthage update --use-xcframeworks && cd Samples && carthage update --use-xcframeworks
```

* For Xcode 11.x or minor:

```bash
carthage update && cd Sample && carthage update
```

---
**NOTES**

Case you have problems with Swift headers not found, when you're trying to install via **carthage** please do the follow instructions: 

 1. Remove Carthage folder from the project
 2. Execute the follow command ```rm -rf ~/Library/Caches/org.carthage.CarthageKit```
 3. This project should add cocoapods folder to the ignore file otherwise carthage will stop working

---